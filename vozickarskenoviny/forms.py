# coding: utf-8

from django import forms

from django_markdown.widgets import MarkdownWidget
from django.utils.safestring import mark_safe

from models import Comment, Article, Page, BazaarItem


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('author_name', 'author_email', 'title', 'text')

class ArticleAdminForm(forms.ModelForm):
    text1 = forms.CharField(widget=MarkdownWidget())
    text2 = forms.CharField(widget=MarkdownWidget())

    class Meta:
        model = Article

class PageAdminForm(forms.ModelForm):
    body = forms.CharField(widget=MarkdownWidget())

    class Meta:
        model = Page


class BazaarItemForm(forms.ModelForm):
    accept_terms = forms.BooleanField(required=True, label=mark_safe('Souhlasím s <a href="/p/pravidla-inzerce/">'
                                                                     'podmínkami inzerce</a>'))

    class Meta:
        model = BazaarItem
        exclude = ('is_proved', )
