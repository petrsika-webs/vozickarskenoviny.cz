from django.conf.urls.defaults import patterns, include, url
from django.views.generic import TemplateView

from views import *

class TextTemplateView(TemplateView):
    def render_to_response(self, context, **response_kwargs):
        response_kwargs['content_type'] = 'text/plain'
        return super(TemplateView, self).render_to_response(context, **response_kwargs)


urlpatterns = patterns('',
    url('^markdown/', include( 'django_markdown.urls')),
    url(r'^robots.txt$', TextTemplateView.as_view(template_name="vozickarskenoviny/robots.txt")),
    url(r'^$',
        home_view,
        name="vn", ),
    url(r'^sekce/(?P<url_key>[a-zA-Z0-9-]+)/$',
        section_view,
        name="vn.section", ),
    url(r'^clanek/(?P<url_key>[a-zA-Z0-9-]+)/$',
        article_view,
        name="vn.article", ),
    url(r'^p/(?P<url_key>[a-zA-Z0-9-]+)/$',
        page_view,
        name="vn.page", ),
    url(r'^bazar/$',
        bazaar_view,
        name="vn.bazaar", ),
    url(r'^bazar/(?P<category>\d+)/$',
        bazaar_view,
        name="vn.bazaar.category", ),
    url(r'^expo/(?P<url_key>\d+)/$',
        expo_view,
        name="vn.expo", ),
    url(r'^sitemap.xml$',
        sitemap_view,
        name="sitemap", ),
)
