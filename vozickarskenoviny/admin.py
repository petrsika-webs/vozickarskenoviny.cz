from django.contrib import admin

from models import Section, Author, Article, Comment, Page, File, BazaarCategory, BazaarItem, Expo, ExpoCategory, ExpoItem

from .forms import ArticleAdminForm, PageAdminForm

class PageAdmin(admin.ModelAdmin):
    form = PageAdminForm

class ArticleAdmin(admin.ModelAdmin):
    form = ArticleAdminForm
    list_display = ('title', 'active', 'date_creation', 'author', 'section', 'reads', )
    readonly_fields = ('reads', )

class CommentAdmin(admin.ModelAdmin):
    list_display = ('title', 'article', 'author_name', 'author_email', 'created_at', )

class FileAdmin(admin.ModelAdmin):
    def file_url(obj):
        return obj.file.url
    list_display = ('name', file_url)


admin.site.register(Article, ArticleAdmin)
admin.site.register(Author)
admin.site.register(Section)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(File, FileAdmin)
admin.site.register(BazaarItem)
admin.site.register(BazaarCategory)
admin.site.register(Expo)
admin.site.register(ExpoItem)
admin.site.register(ExpoCategory)
