import datetime

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.db.models import Sum
from django.core.mail import send_mail
from django.conf import settings

from models import Article, Section, Page, BazaarItem, BazaarCategory, ExpoItem, ExpoCategory, Expo
from forms import CommentForm, BazaarItemForm


def home_view(request, template="vozickarskenoviny/home.html"):
    last_from_section = Section.objects.with_article()
    for i in xrange(len(last_from_section)):
        last_from_section[i].last = last_from_section[i].last_article()
    last_from_section = zip(last_from_section[::2], last_from_section[1::2])

    main_menu = Section.objects.with_article()
    reads_sum = Article.objects.all().aggregate(Sum('reads'))['reads__sum']
    return render_to_response(template,
        {
            "main_menu": main_menu,
            "main_menu_first": main_menu[0:4],
            "main_menu_second": main_menu[4:8],
            "popular3": Article.objects.popularity()[:3],

            'reads_sum': Article.objects.all().aggregate(Sum('reads'))['reads__sum'],
            "last_from_section": last_from_section,
            "last": Article.objects.last(),
            "last3": Article.objects.all()[1:4],
        },
        context_instance=RequestContext(request))


def section_view(request, url_key, template="vozickarskenoviny/section.html"):
    section = Section.objects.get(url_key=url_key)
    articles = Article.objects.all().filter(section=section)
    main_menu = Section.objects.with_article()
    return render_to_response(template,
        {
            'title': section.name,
            "main_menu": main_menu,
            "main_menu_first": main_menu[0:4],
            "main_menu_second": main_menu[4:8],
            'current_section': section,
            "popular3": Article.objects.popularity()[:3],

            "last": articles[0],
            "next": articles[1:],
        },
        context_instance=RequestContext(request))


def page_view(request, url_key, template="vozickarskenoviny/page.html"):
    page = get_object_or_404(Page, url_key=url_key)

    main_menu = Section.objects.with_article()

    return render_to_response(template,
        {
            'title': page.title,
            'page': page,

            "main_menu": main_menu,
            "main_menu_first": main_menu[0:4],
            "main_menu_second": main_menu[4:8],
            "popular3": Article.objects.popularity()[:3],
            'context_only': True,
        },
        context_instance=RequestContext(request))



def article_view(request, url_key, template="vozickarskenoviny/article.html"):
    article = get_object_or_404(Article, url_key=url_key, date_creation__lte=datetime.datetime.now())
    article.reads += 1
    article.save()

    if article.expo:
        return HttpResponseRedirect(reverse('vn.expo', args=(article.expo.pk, )))

    comment_form = CommentForm(request.POST or None)
    if comment_form.is_valid():
        comment = comment_form.save(commit=False)
        comment.article = article
        comment.save()
        return HttpResponseRedirect(reverse("vn.article", args=[article.url_key], ))

    main_menu = Section.objects.with_article()
    return render_to_response(template,
        {
            'title': article.title,
            "main_menu": main_menu,
            "main_menu_first": main_menu[0:4],
            "main_menu_second": main_menu[4:8],
            "comment_form": comment_form,
            'current_section': article.section,
            "popular3": Article.objects.popularity()[:3],

            "article": article,
            "related1": article.get_related()[0:3],
            "related2": article.get_related()[4:7],
            "related3": article.get_related()[8:10],
        },
        context_instance=RequestContext(request))

def sitemap_view(request, template="vozickarskenoviny/sitemap.xml"):
    urls = ['/']
    for section in Section.objects.all():
        urls.append(reverse('vn.section', args=(section.url_key, )))
    for article in Article.objects.all():
        urls.append(reverse('vn.article', args=(article.url_key, )))
    return render_to_response(template,
        {
            "urls": urls,
        },
        content_type="text/xml",
        context_instance=RequestContext(request))


def bazaar_view(request, category=None, template="vozickarskenoviny/bazaar.html"):
    form = BazaarItemForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        obj = form.save()
        send_mail('New bazaar item',
                  'http://vozickarskenoviny.cz/admin/vozickarskenoviny/bazaaritem/%s/' % obj.id,
                  'noreply@vozickarskenoviny.cz',
                  settings.NOTIFY_EMAILS,
                  fail_silently=False)
        return HttpResponseRedirect('?m=done')

    items = BazaarItem.objects.filter(is_proved=True).order_by('-pk')
    if category:
        items = items.filter(category_id=category)
    categories = BazaarCategory.objects.all().order_by('name')

    main_menu = Section.objects.with_article()
    return render_to_response(template, {
            'form': form,
            'items': items,
            'categories': categories,

            'context_only': True,
            "main_menu": main_menu,
            "main_menu_first": main_menu[0:4],
            "main_menu_second": main_menu[4:8],
        },
        context_instance=RequestContext(request))


def expo_view(request, url_key, template="vozickarskenoviny/expo.html"):
    expo = get_object_or_404(Expo, url_key=url_key)
    items = expo.expoitem_set.all()
    category = int(request.GET.get('category', 0))
    if category:
        items = items.filter(categories__in=[category])
    categories = []

    for item in items:
        for cat in item.categories.all():
            if cat not in categories:
                categories.append(cat)

    main_menu = Section.objects.with_article()
    return render_to_response(template, {
        'expo': expo,
        'items': items,
        'categories': categories,

        'context_only': True,
        "main_menu": main_menu,
        "main_menu_first": main_menu[0:4],
        "main_menu_second": main_menu[4:8],
    }, context_instance=RequestContext(request))
