# coding: utf-8

import datetime

from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.core.urlresolvers import reverse


class SectionManager(models.Manager):
    def all(self):
        return self.order_by('order', "name")

    def with_article(self):
        return filter(None, (section if section.article_set.all().count() > 0 else None for section in self.all()))


class Section(models.Model):
    objects = SectionManager()

    name = models.CharField(max_length=32)
    url_key = models.SlugField(max_length=32, blank=True, unique=True)

    order = models.IntegerField(default=1)

    def last_article(self):
        return self.article_set.last()

    def last_7_articles(self):
        return self.article_set.all()[:7]

    def last1(self):
        return self.article_set.last1()

    def __unicode__(self):
        return u"%s"%self.name

    def save(self, *args, **kwargs):
        if not self.url_key:
            self.url_key = slugify(self.name)
        super(Section, self).save(*args, **kwargs)

class Author(User):
    pass

class ArticleManager(models.Manager):
    def all(self):
        return self.filter(active=True, date_creation__lte=datetime.datetime.now()).order_by("-date_creation", "-pk")

    def last(self, i=0):
        try:
            return self.all()[i]
        except IndexError:
            return None

    def last1(self):
        return self.last(1)

    def popularity(self):
        return self.all().order_by("-reads", "-date_creation", "-pk")


class Article(models.Model):
    objects = ArticleManager()

    reads = models.IntegerField(default=0)
    date_creation = models.DateTimeField(default=datetime.datetime.now)
    date_actualization = models.DateTimeField(default=datetime.datetime.now)
    show_actualization = models.BooleanField()
    active = models.BooleanField()
    author = models.ForeignKey(Author)
    section = models.ForeignKey(Section)

    title = models.CharField(max_length=255)
    url_key = models.SlugField(blank=True, unique=True)
    perex = models.TextField()
    text1 = models.TextField()
    text2 = models.TextField()

    keywords = models.CharField(max_length=255)

    expo = models.ForeignKey('Expo', null=True, blank=True)

    img = models.ImageField(upload_to="imgs")
    img_author = models.CharField(max_length=64, default='', blank=True)
    img_description = models.CharField(max_length=255, default='', blank=True)
    video = models.CharField(blank=True, null=True, help_text=u'YouTube video ID (e.g. f1U0beJtPPI)', max_length=16)

    img1 = models.ImageField(upload_to="imgs", blank=True, null=True)
    img1_author = models.CharField(max_length=64, default='', blank=True)
    img1_description = models.CharField(max_length=255, default='', blank=True)
    img2 = models.ImageField(upload_to="imgs", blank=True, null=True)
    img2_author = models.CharField(max_length=64, default='', blank=True)
    img2_description = models.CharField(max_length=255, default='', blank=True)
    img3 = models.ImageField(upload_to="imgs", blank=True, null=True)
    img3_author = models.CharField(max_length=64, default='', blank=True)
    img3_description = models.CharField(max_length=255, default='', blank=True)
    img4 = models.ImageField(upload_to="imgs", blank=True, null=True)
    img4_author = models.CharField(max_length=64, default='', blank=True)
    img4_description = models.CharField(max_length=255, default='', blank=True)
    img5 = models.ImageField(upload_to="imgs", blank=True, null=True)
    img5_author = models.CharField(max_length=64, default='', blank=True)
    img5_description = models.CharField(max_length=255, default='', blank=True)
    img6 = models.ImageField(upload_to="imgs", blank=True, null=True)
    img6_author = models.CharField(max_length=64, default='', blank=True)
    img6_description = models.CharField(max_length=255, default='', blank=True)

    def __unicode__(self):
        return u"(%i) (%s) %s" % (self.pk, self.author, self.title[:50])

    def get_related(self):
        return Article.objects.all().filter(section=self.section, active=True).exclude(pk=self.pk)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.url_key = slugify(self.title)
        self.date_actualization = datetime.datetime.now()
        return super(Article, self).save(*args, **kwargs)

    def get_comments(self):
        return self.comment_set.order_by("pk")

    class Meta:
        ordering = ('-date_creation', )


class Comment(models.Model):
    article = models.ForeignKey(Article)
    created_at = models.DateField(auto_now_add=True)

    author_name = models.CharField(max_length=64)
    author_email = models.CharField(max_length=64)

    title = models.CharField(max_length=255)
    text = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return u"%s %i %s" % (self.title[:30], self.pk, self.article.title)


class Page(models.Model):
    title = models.CharField(max_length=255)
    url_key = models.SlugField(max_length=255, unique=True)
    body = models.TextField()

    def __unicode__(self):
        return u'%s' % self.title

    def get_absolute_url(self):
        return reverse('vn.page', args=(self.url_key, ))


class File(models.Model):
    name = models.CharField(max_length=255)
    file = models.FileField(upload_to='vozickarskenoviny/file/file')

    def __unicode__(self):
        return u'%s %s' % (self.name, self.file.url)


class BazaarCategory(models.Model):
    name = models.CharField(max_length=64)

    def __unicode__(self):
        return u'%s' % self.name


class BazaarItem(models.Model):
    category = models.ForeignKey(BazaarCategory, verbose_name='Kategorie')
    title = models.CharField(max_length=255, verbose_name='Nadpis')
    description = models.TextField(verbose_name='Popis')
    email = models.EmailField()
    phone = models.CharField(max_length=16, default='', blank=True, verbose_name='Telefon')
    img = models.ImageField(upload_to="vozickarskenoviny/bazaaritem/img", blank=True, null=True,
                            verbose_name='Obrázek')
    is_proved = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%s' % self.title


class ExpoCategory(models.Model):
    name = models.CharField(max_length=64)

    def __unicode__(self):
        return u'%s' % self.name


class Expo(models.Model):
    title = models.CharField(max_length=64)
    url_key = models.CharField(max_length=64)
    text = models.TextField()
    img = models.ImageField(upload_to='vn/expo/img')

    def __unicode__(self):
        return u'%s' % self.title


class ExpoItem(models.Model):
    expo = models.ForeignKey(Expo)
    title = models.CharField(max_length=64)
    text = models.TextField()
    categories = models.ManyToManyField(ExpoCategory)
    phone = models.CharField(max_length=64)
    www = models.URLField()
    email = models.EmailField()
    link = models.URLField()
    img = models.ImageField(upload_to='vn/expo/img')

    def __unicode__(self):
        return u'%s' % self.title
